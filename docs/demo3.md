# Demo 3 - RabbitMQ

Let's shut down the `RabbitMQ` instance and see what happens.

`https://localhost:5001/> GET health`

```http
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: text/plain
Date: Sun, 25 Jul 2021 12:54:12 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

Degraded
```

And when the `RabbitMQ` instance is available:

`https://localhost:5001/> GET health`

```htttp
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: text/plain
Date: Sun, 25 Jul 2021 12:55:07 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

Healthy
```
