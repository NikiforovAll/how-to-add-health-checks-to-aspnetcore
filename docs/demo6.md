# Demo 6 - Customize ouput demo

Let's shut down all dependencies.

`https://localhost:5001/> GET health`

```http
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: application/json; charset=utf-8
Date: Sun, 25 Jul 2021 14:50:23 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

{
  "status": "Healthy",
  "results": {
  }
}
```

`https://localhost:5001/> GET health/ready`

```http
HTTP/1.1 503 Service Unavailable
Cache-Control: no-store, no-cache
Content-Type: application/json; charset=utf-8
Date: Sun, 25 Jul 2021 14:51:09 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

{
  "status": "Unhealthy",
  "results": {
    "Database": {
      "status": "Degraded"
    },
    "RabbitMQ": {
      "status": "Degraded"
    },
    "Downstream API Health Check": {
      "status": "Unhealthy"
    }
  }
}
```

Let's star up all dependencies.

`https://localhost:5001/> GET health`

```http
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: application/json; charset=utf-8
Date: Sun, 25 Jul 2021 14:52:11 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

{
  "status": "Healthy",
  "results": {
  }
}
```

`https://localhost:5001/> GET health/ready`

```http
HTTP/1.1 200 OK
Cache-Control: no-store, no-cache
Content-Type: application/json; charset=utf-8
Date: Sun, 25 Jul 2021 14:51:52 GMT
Expires: Thu, 01 Jan 1970 00:00:00 GMT
Pragma: no-cache
Server: Kestrel
Transfer-Encoding: chunked

{
  "status": "Healthy",
  "results": {
    "Database": {
      "status": "Healthy"
    },
    "RabbitMQ": {
      "status": "Healthy"
    },
    "Downstream API Health Check": {
      "status": "Healthy"
    }
  }
}
```
