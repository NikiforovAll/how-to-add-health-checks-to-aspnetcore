namespace ASPNETCore.HowTos.Healthchecks
{
    using System;
    using System.Data.Common;
    using System.Threading;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Diagnostics.HealthChecks;

    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = this.Configuration.GetConnectionString("DefaultConnection");
            var rabbitMqConnectionString = this.Configuration.GetConnectionString("RabbitMQ");
            var downstreamServiceUrl = this.Configuration["DownstreamService:BaseUrl"];
            services.AddHealthChecks()
                .AddSqlServer(
                    connectionString,
                    name: "Database",
                    failureStatus: HealthStatus.Degraded,
                    timeout: TimeSpan.FromSeconds(1),
                    tags: new string[] { "services" })
                .AddRabbitMQ(
                    rabbitMqConnectionString,
                    name: "RabbitMQ",
                    failureStatus: HealthStatus.Degraded,
                    timeout: TimeSpan.FromSeconds(1),
                    tags: new string[] { "services" })
                .AddUrlGroup(
                    new Uri($"{downstreamServiceUrl}/health"),
                    name: "Downstream API Health Check",
                    failureStatus: HealthStatus.Unhealthy,
                    timeout: TimeSpan.FromSeconds(3),
                    tags: new string[] { "services" });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapCustomHealthCheck();

                endpoints.MapGet("/{**path}", async context =>
                {
                    await context.Response.WriteAsync(
                        "Navigate to /health to see the health status.");
                });
            });
        }
    }
}
